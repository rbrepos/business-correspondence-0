function activateSortable() {
  //Sortable block
  // stage 1
  new Sortable(document.querySelector(".stage1_test_sort_answer_list"), {
    group: "shared", // set both lists to same group
    animation: 150
  });

  new Sortable(document.querySelector(".stage1_test_sort1"), {
    group: "shared",
    animation: 150
  });

  new Sortable(document.querySelector(".stage1_test_sort2"), {
    group: "shared",
    animation: 150
  });

  new Sortable(document.querySelector(".stage1_test_sort3"), {
    group: "shared",
    animation: 150
  });

  new Sortable(document.querySelector(".stage1_test_sort4"), {
    group: "shared",
    animation: 150
  });
  // stage 2_1
  new Sortable(document.querySelector(".stage2-1_test_sort_answer_list"), {
    group: "stage2_1", // set both lists to same group
    animation: 150
  });

  new Sortable(document.querySelector(".stage2-1_test_sort1"), {
    group: "stage2_1",
    animation: 150
  });

  new Sortable(document.querySelector(".stage2-1_test_sort2"), {
    group: "stage2_1",
    animation: 150
  });

  new Sortable(document.querySelector(".stage2-1_test_sort3"), {
    group: "stage2_1",
    animation: 150
  });

  // stage 2_3
  new Sortable(document.querySelector(".stage2-3_test_sort_answer_list"), {
    group: "stage2_3", // set both lists to same group
    animation: 150
  });

  document.querySelectorAll(".stage2-3_test_sort").forEach(function(elem) {
    new Sortable(elem, {
      group: "stage2_3",
      animation: 150
    });
  });

  // stage 3
  new Sortable(document.querySelector(".stage3-1_test_sort_answer_list"), {
    group: "stage3_1", // set both lists to same group
    animation: 150
  });

  new Sortable(document.querySelector(".stage3-1_test_sort1"), {
    group: "stage3_1",
    animation: 150
  });

  new Sortable(document.querySelector(".stage3-1_test_sort2"), {
    group: "stage3_1",
    animation: 150
  });

  new Sortable(document.querySelector(".stage3-1_test_sort3"), {
    group: "stage3_1",
    animation: 150
  });

  new Sortable(document.querySelector(".stage3-1_test_sort4"), {
    group: "stage3_1",
    animation: 150
  });
  // stage 3_2
  new Sortable(document.querySelector(".stage3-2_test_sort_answer_list"), {
    group: "stage3_2", // set both lists to same group
    animation: 150
  });

  new Sortable(document.querySelector(".stage3-2_test_sort1"), {
    group: "stage3_2",
    animation: 150
  });

  new Sortable(document.querySelector(".stage3-2_test_sort2"), {
    group: "stage3_2",
    animation: 150
  });

  new Sortable(document.querySelector(".stage3-2_test_sort3"), {
    group: "stage3_2",
    animation: 150
  });
  // stage 4_1
  new Sortable(document.querySelector(".stage4-1_test_sort_answer_list"), {
    group: "stage4_1", // set both lists to same group
    animation: 150
  });

  new Sortable(document.querySelector(".stage4-1_test_sort1"), {
    group: "stage4_1",
    animation: 150
  });

  new Sortable(document.querySelector(".stage4-1_test_sort2"), {
    group: "stage4_1",
    animation: 150
  });

  new Sortable(document.querySelector(".stage4-1_test_sort3"), {
    group: "stage4_1",
    animation: 150
  });

  new Sortable(document.querySelector(".stage4-1_test_sort4"), {
    group: "stage4_1",
    animation: 150
  });
  // stage 4_2
  new Sortable(document.querySelector(".stage4-2_test_sort_answer_list"), {
    group: "stage4_2", // set both lists to same group
    animation: 150
  });

  new Sortable(document.querySelector(".stage4-2_test_sort1"), {
    group: "stage4_2",
    animation: 150
  });

  new Sortable(document.querySelector(".stage4-2_test_sort2"), {
    group: "stage4_2",
    animation: 150
  });

  new Sortable(document.querySelector(".stage4-2_test_sort3"), {
    group: "stage4_2",
    animation: 150
  });

  new Sortable(document.querySelector(".stage4-2_test_sort4"), {
    group: "stage4_2",
    animation: 150
  });
  //stage5
  new Sortable(document.querySelector(".stage5_test_sort1"), {
    group: "stage5",
    animation: 150
  });
  new Sortable(document.querySelector(".stage5_test_sort2"), {
    group: "stage5",
    animation: 150
  });
  new Sortable(document.querySelector(".stage5_test_sort3"), {
    group: "stage5",
    animation: 150
  });
  new Sortable(document.querySelector(".stage5_test_sort_answer_list"), {
    group: "stage5", // set both lists to same group
    animation: 150
  });
  //stage 8
  new Sortable(document.querySelector(".stage8_test_sort_answer_list"), {
    group: "stage8", // set both lists to same group
    animation: 150
  });

  new Sortable(document.querySelector(".stage8_test_sort1"), {
    group: "stage8",
    animation: 150
  });

  new Sortable(document.querySelector(".stage8_test_sort2"), {
    group: "stage8",
    animation: 150
  });

  new Sortable(document.querySelector(".stage8_test_sort3"), {
    group: "stage8",
    animation: 150
  });

  new Sortable(document.querySelector(".stage8_test_sort4"), {
    group: "stage8",
    animation: 150
  });
  new Sortable(document.querySelector(".stage9_test_sort1"), {
    group: "stage9",
    animation: 150
  });
  new Sortable(document.querySelector(".stage9_test_sort2"), {
    group: "stage9",
    animation: 150
  });
  new Sortable(document.querySelector(".stage9_test_sort3"), {
    group: "stage9",
    animation: 150
  });
  new Sortable(document.querySelector(".stage9_test_sort4"), {
    group: "stage9",
    animation: 150
  });
  new Sortable(document.querySelector(".stage9_test_sort_answer_list"), {
    group: "stage9",
    animation: 150
  });
  // stage 10_3
  new Sortable(document.querySelector(".stage10-1_test_sort_answer_list"), {
    group: "stage10_1", // set both lists to same group
    animation: 150
  });

  new Sortable(document.querySelector(".stage10-1_test_sort1"), {
    group: "stage10_1",
    animation: 150
  });

  new Sortable(document.querySelector(".stage10-1_test_sort2"), {
    group: "stage10_1",
    animation: 150
  });

  new Sortable(document.querySelector(".stage10-1_test_sort3"), {
    group: "stage10_1",
    animation: 150
  });

  new Sortable(document.querySelector(".stage10-1_test_sort4"), {
    group: "stage10_1",
    animation: 150
  });
  /// Sortable end
}
