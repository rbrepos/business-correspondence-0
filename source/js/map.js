(function () {
  console.log(1);
  // timer
  var timerObserver = new IntersectionObserver(function (entries, tObserver) {
    entries.forEach(function (entry) {
      if (entry.isIntersecting) {
        var timer = entry.target;
        var cnt = timer.querySelector(".final__timer-disapear-countdown");

        var timeleft = 9;
        var downloadTimer = setInterval(function () {
          cnt.innerHTML = "00:0" + timeleft;
          timeleft -= 1;
          if (timeleft < -1) {
            timer.classList.add("visually-hidden");
            clearInterval(downloadTimer);
          }
          if (timeleft < 0) {
            // timer.classList.add('final__timer-disapear-countdown');
            document
              .querySelector(".final__timer")
              .classList.add("final__timer--hidden");
            cnt.innerHTML = "00:00";
          }
        }, 1000);
        tObserver.unobserve(timer);
      }
    });
  });
  timerObserver.observe(document.querySelector(".final__timer"));

  var calendarHowTo = document.querySelector(".signup__calendar");
  var calendarHowToClick = function (evt) {
    evt.preventDefault();
    var howTo = document.querySelector(".calendar-howto");
    howTo.classList.contains("visually-hidden")
      ? howTo.classList.remove("visually-hidden")
      : howTo.classList.add("visually-hidden");
    //document.querySelector('.calendar-howto').classList.remove('visually-hidden');
    // calendarHowTo.removeEventListener(calendarHowToClick);
  };
  calendarHowTo.addEventListener("click", calendarHowToClick);
})();
