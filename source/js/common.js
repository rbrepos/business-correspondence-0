// check JS support
var nojs = document.querySelector(".nojs");
nojs.classList.remove("nojs");
nojs.classList.add("js");

// check webp support
var supportsWebP = (function () {
  "use strict";

  var canvas =
    typeof document === "object" ? document.createElement("canvas") : {};
  canvas.width = canvas.height = 1;
  var index = canvas.toDataURL
    ? canvas.toDataURL("image/webp").indexOf("image/webp") === 5
    : false;

  return index;
})();

if (supportsWebP) {
  var webpAll = document.querySelectorAll(".nowebp");
  for (var i = 0; i < webpAll.length; i++) {
    var webp = webpAll[i];
    webp.classList.remove("nowebp");
    webp.classList.add("webp");
  }
}

// Lazy images
var images = window.document.querySelectorAll("img, source, iframe, .nobg");
var config = {
  // If the image gets within 50px in the Y axis, start the download.
  rootMargin: "0px 50px",
  threshold: 0.01,
};

var preloadImage = function (element) {
  if (element.dataset && element.dataset.src) {
    element.src = element.dataset.src;
    // refersh для того чтобы пересчитался первый слайд
    // $(".slider_wrapper").slick('refresh');
  }
  if (element.dataset && element.dataset.srcset) {
    element.srcset = element.dataset.srcset;
  }
  if (element.classList.contains("nobg")) {
    element.classList.remove("nobg");
    element.classList.add("withbg");
  }
};

// проверить есть ли IntersectionObserver, если нет то грузим сразу все картинки
if (!("IntersectionObserver" in window)) {
  Array.from(images).forEach(function (image) {
    preloadImage(image);
  });
} else {
  var observer = new IntersectionObserver(function (entries) {
    entries.forEach(function (entry) {
      if (entry.intersectionRatio > 0) {
        // Stop watching and load the image
        observer.unobserve(entry.target);
        // call our method: preloadImage
        preloadImage(entry.target);
      }
    });
  }, config);
  images.forEach(function (image) {
    observer.observe(image);
  });
}

// Nodelist forEach
// https://developer.mozilla.org/en-US/docs/Web/API/NodeList/forEach
if (window.NodeList && !NodeList.prototype.forEach) {
  NodeList.prototype.forEach = function (callback, thisArg) {
    thisArg = thisArg || window;
    for (var i = 0; i < this.length; i++) {
      callback.call(thisArg, this[i], i, this);
    }
  };
}

// get element coords
var getCoords = function (elem) {
  // кроме IE8-
  var box = elem.getBoundingClientRect();

  return {
    top: box.top + pageYOffset,
    left: box.left + pageXOffset,
  };
};

// (function() {
//   try {
//     var parentUrl = new URL(document.referrer);
//     var course_id = parentUrl.searchParams.get('course_id');
//   } catch(err_) {
//     var course_id = 100001;
//   }
//   var REST_URL = '/webtutor/course_view_time/rest.html';
//   var getElementPath = function(el){
//     var path = el.nodeName + '.' + (el.className ? el.className : '');
//     var parent = el.parentNode;
//     while(parent){
//       path = parent.nodeName + (parent.className !== undefined && parent.className !== '' ? '.' + parent.className : '') + '/' + path;
//       parent = parent.parentNode;
//     }
//     console.log(path);
//     return path;
//   }

//   if (('IntersectionObserver' in window)) {
//     var options = {
//       // родитель целевого элемента - область просмотра
//       root: null,
//       // без отступов
//       rootMargin: '0px',
//       // процент пересечения
//       // threshold: [0, 0.2, 0.4, 0.6, 0.8, 1]
//       threshold: [0.5, 0.8]
//     }
//     var observer = new IntersectionObserver((entries, observer) => {
//       // для каждой записи-целевого элемента
//       entries.forEach(entry => {
//           //let header_name;
//           // если элемент является наблюдаемым
//           if (entry.isIntersecting) {
//               if(entry.intersectionRatio >= 0.8) {
//                 // if(!(+entry.target.dataset['InView']) === 1) {
//                   //console.log(entry.target, entry.intersectionRatio, 'inView');
//                   entry.target.dataset['isSended'] = 1;
//                   entry.target.dataset['inViewTime'] = new Date().getTime();
//                 // }
//               }
//               if(entry.intersectionRatio < 0.8 && entry.target.dataset['isSended'] == 1) {
//                 // if(!(+entry.target.dataset['InView']) === 0) {
//                   //console.log(entry.target, entry.intersectionRatio, 'outOfView');
//                   entry.target.dataset['isSended'] = 0;
//                   //console.log((new Date().getTime() - +entry.target.dataset['inViewTime']) / 1000);
//                   //console.log(STATE.currentPart);
//                   //console.log(getElementPath(entry.target));
//                 // }
//                 // console.log(entry.target, entry.intersectionRatio, 'OutOfView');
//                 let header_name;
//                 const headers = entry.target.querySelectorAll('h1, h2, h3');
//                 if(headers.length > 0) {
//                   header_name = headers[0].textContent;
//                 } else {
//                   header_name = '';
//                 }
//                 $.ajax({
//                   type: "POST",
//                   url: REST_URL,
//                   data: {
//                     course_id: course_id,
//                     view_time: ((new Date().getTime() - +entry.target.dataset['inViewTime']) / 1000),
//                     html_path: getElementPath(entry.target),
//                     part_id: STATE.currentPart,
//                     header_name: header_name
//                   },
//                   success: () => {
//                     console.log('Data recorded');
//                   },
//                   // dataType: dataType
//                 });
//               }
//           }
//       })
//     }, options);
//     document.querySelectorAll('section').forEach(function(it) {
//       observer.observe(it);
//     });
//   }
// })();
