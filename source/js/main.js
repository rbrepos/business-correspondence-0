//=require node_modules/jquery/dist/jquery.min.js
//=require source/js/polyfills.js

//For IE
// function getInternetExplorerVersion() {
//   var rv = -1;
//   if (navigator.appName != "Microsoft Internet Explorer") {
//     var ua = navigator.userAgent;
//     var re = new RegExp("MSIE ([0-9]{1,}[\\.0-9]{0,})");
//     if (re.exec(ua) != null) rv = parseFloat(RegExp.$1);
//   } else if (navigator.appName == "Netscape") {
//     var ua = navigator.userAgent;
//     var re = new RegExp("Trident/.*rv:([0-9]{1,}[\\.0-9]{0,})");
//     if (re.exec(ua) != null) rv = parseFloat(RegExp.$1);
//   }
//   return rv;
// }

// alert(window.navigator.appName);
function getRefererUrl() {
  if (document.referrer) {
    var myReferer = document.referrer;
    var svalue = myReferer.match(new RegExp("[?&]object_id=([^&]*)(&?)", "i"));
    //var res = svalue ? svalue[1] : svalue;
    //console.log(res)
    return svalue ? svalue[1] : svalue;
  } else {
    return "";
  }
}


// $(document).ready(function(){
$(function () {
  var isIE = /*@cc_on!@*/false || !!document.documentMode;
  if (isIE) {
    // var r = document.getElementsByClassName('root');
    // r.style.display = 'none';
    $("#modal_ie").show();
    // $('.copy-block__input').val(window.location);
    $(".copy-block__input").val(
      "http://webtutor.rosbank.rus.socgen:81/view_doc.html?mode=learning_proc&object_id=" +
      getRefererUrl()
    );
    console.log(window.location);
    $(".copy-block__btn").on("click", function () {
      var value = document.querySelector(".copy-block__input");
      value.select();

      // Copy the highlighted text
      document.execCommand("copy");
    });
  }
});
//For IE end
// Intersection Observer
// //=require node_modules/intersection-observer/intersection-observer.js

// Common JS (Lazy loading, check webp, etc...)
//=require source/js/common.js

// Scorm API
//=require source/js/SCORM_API_wrapper.js

// Sortable
// //=require source/js/sortable.js

// cmi.suspend_data (CMIString (SPM: 4096), RW) Provides space to store and retrieve data between learner sessions

STATE = {
  maxScore: 30,
  minScore: 0,
  normalScore: 1,
  cntBlockAnswer: 0,
  score: {},
  suspend_data: {},
  currentPart: 0,
  stageComplete: [-1],
  partQuestionsNum: 3,
};
// ////////SCORM Initialisate

pipwerks.SCORM.connection.initialize();
pipwerks.SCORM.data.set("cmi.score.min", STATE.minScore);
pipwerks.SCORM.data.set("cmi.score.max", STATE.maxScore);
STATE.score.raw = pipwerks.SCORM.data.get("cmi.score.raw") || 0;
STATE.score.scaled = pipwerks.SCORM.data.get("cmi.score.scaled") || 0;
STATE.suspend_data = {};
// доп данные которые сохраняем в течении сессии
if (pipwerks.SCORM.data.get("cmi.suspend_data") !== "") {
  STATE.suspend_data =
    JSON.parse(pipwerks.SCORM.data.get("cmi.suspend_data")) || {};
}
try {
  STATE.currentPart = STATE.suspend_data.currentPart || 0;
} catch (err_) {
  STATE.currentPart = 0;
}
try {
  STATE.stageComplete = STATE.suspend_data.stageComplete || [-1];
} catch (err_) {
  STATE.stageComplete = [-1];
}

// DEV MODE
// STATE.currentPart = 2;
// STATE.suspend_data = {
//   "answered":[{"questionId":"12","part":4,"isCorrect":false,"answers":["stage4-test-radio6","stage4-test-radio7"]},
//     {"questionId":"8","part":"3","isCorrect":false,"answers":["stage3-test-radio4"]},
//     {"questionId":"15","part":"5","isCorrect":false,"answers":["stage5-test-radio5","stage5-test-radio6"]},
//     {"questionId":"2","part":"1","isCorrect":true,"answers":["stage1-test-radio6"]},
//     {"questionId":"10","part":4,"isCorrect":false}],
//   "currentPart":"1"};

var calcScaledScore = function (raw) {
  return (raw / STATE.maxScore).toPrecision(4);
};

function saveStateToScorm() {
  pipwerks.SCORM.data.set("cmi.score.raw", STATE.score.raw);
  pipwerks.SCORM.data.set("cmi.score.scaled", STATE.score.scaled);
  pipwerks.SCORM.data.set("cmi.suspend_data", "");
  pipwerks.SCORM.save();
  pipwerks.SCORM.data.set(
    "cmi.suspend_data",
    JSON.stringify(STATE.suspend_data)
  );
  pipwerks.SCORM.save();
}

function updateCompleteScore() {
  var score, totalScore;
  try {
    score = STATE.suspend_data.answered
      .filter(function (it) {
        return it.isCorrect === true && it.part === STATE.currentPart;
      })
      .map(function (it) {
        return 1;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);
  } catch (err_) {
    score = 0;
  }
  try {
    totalScore = STATE.suspend_data.answered
      .filter(function (it) {
        return it.isCorrect === true;
      })
      .map(function (it) {
        return 1;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);
  } catch (err_) {
    totalScore = 0;
  }
  // document.querySelector(
  //   ".stage-page--" + STATE.currentPart + " .total-stage-score"
  // ).innerHTML = score;
  // document.querySelector(
  //   ".stage-page--" + STATE.currentPart + " .complete-score"
  // ).innerHTML = totalScore;
  return totalScore;
}

try {
  STATE.suspend_data.answered.forEach(function (it, i) {
    console.log(it);
  });
} catch (er) { }

function updateMapScore() {
  var totalScore;
  try {
    totalScore = STATE.suspend_data.answered
      .filter(function (it) {
        return it.isCorrect === true;
      })
      .map(function (it) {
        return 1;
      })
      .reduce(function (a, b) {
        return a + b;
      }, 0);
  } catch (err_) {
    totalScore = 0;
  }
  document.querySelector(".map__score-gained").innerHTML = totalScore;
  // debugger;
  for (var i = 1; i < 11; i++) {
    var score, scoreFailed;
    try {
      score = STATE.suspend_data.answered
        .filter(function (it) {
          return it.isCorrect === true && parseInt(it.part, 10) === i;
        })
        .map(function (it) {
          return 1;
        })
        .reduce(function (a, b) {
          return a + b;
        }, 0);
      scoreFailed = STATE.suspend_data.answered
        .filter(function (it) {
          return it.isCorrect === false && parseInt(it.part, 10) === i;
        })
        .map(function (it) {
          return 1;
        })
        .reduce(function (a, b) {
          return a + b;
        }, 0);
    } catch (err_) {
      score = 0;
      scoreFailed = 0;
    }
    console.log(score, scoreFailed);
    var scoreElem = document.querySelector(".stages__score--" + i + " span");
    scoreElem.innerHTML = score;
    if (score > 2) {
      try {
        scoreElem.parentElement.parentElement
          .querySelector(".person__avatar--passed")
          .classList.remove("person__avatar--hidden");
        scoreElem.parentElement.parentElement
          .querySelector(".person__avatar--halfpassed")
          .classList.add("person__avatar--hidden");
        scoreElem.parentElement.parentElement
          .querySelector(".person__avatar--notpassed")
          .classList.add("person__avatar--hidden");
      } catch (err_) {
        console.error(err_);
      }
    } else if (scoreFailed || (score > 0 && score <= 2)) {
      // console.log(score);
      scoreElem.parentElement.parentElement
        .querySelector(".person__avatar--passed")
        .classList.add("person__avatar--hidden");
      scoreElem.parentElement.parentElement
        .querySelector(".person__avatar--halfpassed")
        .classList.remove("person__avatar--hidden");
      scoreElem.parentElement.parentElement
        .querySelector(".person__avatar--notpassed")
        .classList.add("person__avatar--hidden");
    }
  }
  // return totalScore;
}

// if (parseInt(STATE.currentPart, 10) > 0) {
//   updateCompleteScore();
// } else {
//   updateMapScore();
// }

var stagePages = document.querySelectorAll(".stage-page");
var mainHeader = document.querySelector(".main-header");

// var showCurrentStage = function () {
//   stagePages.forEach(function (page) {
//     page.style = "display: none;";
//     // page.classList.add('visually-hidden');
//   });

//   mainHeader.style = "display: none;";
//   // mainHeader.classList.add('visually-hidden');
//   window.scrollTo(0, 0);

//   document.querySelector(".stage-page--" + STATE.currentPart).style =
//     "display: block;";
//   // $(".slider_wrapper").slick('refresh');
//   STATE.stageComplete.push(parseInt(STATE.currentPart));
//   STATE.suspend_data.stageComplete = STATE.stageComplete;
//   saveStateToScorm();
//   // document.querySelector('.stage-page--' + STATE.currentPart).classList.remove('visually-hidden');
//   if (parseInt(STATE.currentPart, 10) === 0) {
//     mainHeader.style = "display: block;";
//     // mainHeader.classList.remove('visually-hidden');
//   }
//   if (parseInt(STATE.currentPart, 10) > 0) {
//     updateCompleteScore();
//   } else {
//     updateMapScore();
//   }
// };

var chkAnsweredAll = function (stage) {
  var isAnsweredAll = true;
  try {
    isAnsweredAll =
      STATE.suspend_data.answered.filter(function (it) {
        var curStage = Number.parseInt(stage, 10);
        return curStage === Number.parseInt(it.part, 10);
      }).length === STATE.partQuestionsNum;
  } catch (err) {
    isAnsweredAll = false;
  }
  return isAnsweredAll;
};

// // pipwerks.SCORM.data.set('cmi.success_status', 'passed');
// // pipwerks.SCORM.data.set('cmi.completion_status', 'completed');
// // pipwerks.SCORM.data.save();
// // pipwerks.SCORM.data.set('cmi.score.scaled', 0.8);
// // pipwerks.SCORM.data.save();

document.addEventListener("DOMContentLoaded", function () {
  // showCurrentStage();

  // var stagesLinks = document.querySelectorAll(".stages__link");
  // // console.log(stagesLinks);
  // var stagesLinkOnClick = function (evt) {
  //   // console.log(STATE.currentPart);
  //   // console.log(STATE.suspend_data.answered);
  //   // console.log(STATE.stageComplete);
  //   // console.log(
  //   //   STATE.stageComplete.filter(function (i) {
  //   //     return i == parseInt(evt.target.dataset.stage, 10) - 1;
  //   //   })
  //   // );
  //   evt.preventDefault();
  //   evt.stopPropagation();
  //   var isCanNext =
  //     STATE.stageComplete.filter(function (i) {
  //       return i == parseInt(evt.target.dataset.stage, 10) - 1;
  //     }).length > 0;
  //   var isAnsweredAll = true;
  //   // debugger;

  //   // если не возврат на карту
  //   var nextStage = parseInt(evt.target.dataset.stage, 10);
  //   if (nextStage !== 0 && nextStage > Math.max(...STATE.stageComplete)) {
  //     var stagesArr = STATE.stageComplete.filter(function (it) {
  //       return it > 0;
  //     });
  //     for (var i = 0; i < stagesArr.length; i++) {
  //       if (!chkAnsweredAll(stagesArr[i])) {
  //         isAnsweredAll = false;
  //         break;
  //       }
  //     }
  //   }
  //   if (isCanNext && isAnsweredAll) {
  //     STATE.currentPart = evt.target.dataset.stage;
  //     STATE.suspend_data.currentPart = evt.target.dataset.stage;

  //     // pipwerks.SCORM.data.set('cmi.suspend_data', JSON.stringify(STATE.suspend_data));
  //     // pipwerks.SCORM.save();
  //     saveStateToScorm();
  //     // showCurrentStage();
  //     if (STATE.currentPart == "0") {
  //       try {
  //         evt.preventDefault();
  //         window.scrollTo({
  //           top: getCoords(document.querySelector(evt.target.dataset.scrollto))
  //             .top,
  //           behavior: "smooth",
  //         });
  //       } catch (err_) {
  //         console.log(err_);
  //       }
  //     }
  //   } else {
  //     if (!isCanNext) {
  //       tingleNextStage.open();
  //     } else {
  //       if (!isAnsweredAll) {
  //         tingleNotAllAnswers.open();
  //       }
  //     }
  //   }
  // };

  // stagesLinks.forEach(function (stageLink) {
  //   stageLink.addEventListener("click", stagesLinkOnClick);
  // });

  tingleStage7 = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ["overlay", "button", "escape"],
    closeLabel: "Close",
    cssClass: ["stage7-modal"],
  });
  tingleStage7.setContent(
    '<section class="stage7-popup-grid">' +
    '  <div class="stage7-popup-grid__cell stage7-popup-grid__cell--1">' +
    '    <p class="stage7-popup-grid__header">Подарки</p>' +
    "  </div>" +
    '  <div class="stage7-popup-grid__cell stage7-popup-grid__cell--2">' +
    "    <p>Проведение деловых обедов и мероприятий с участием должностных лиц* запрещена по общему правилу.</p>" +
    "    <p>В случае получения или преподнесения подарков, проведении деловых обедов и мероприятий с участием должностных лиц требуется предварительное согласие со стороны Департамента внутреннего контроля.</p>" +
    "  </div>" +
    '  <div class="stage7-popup-grid__cell stage7-popup-grid__cell--3">' +
    '    <p class="stage7-popup-grid__lead">< 8000 руб.</p>' +
    '    <p class="stage7-popup-grid__lead">≤ 8000 руб.</p>' +
    "    <p>если должностное лицо</p>" +
    "  </div>" +
    '  <div class="stage7-popup-grid__cell stage7-popup-grid__cell--4">' +
    '    <p class="stage7-popup-grid__lead">≥ 8000 руб.</p>' +
    "    <p>Требуется предварительное согласование непосредственного руководителя и Департамента внутреннего контроля, внесение в" +
    "    реестр.</p>" +
    "  </div>" +
    '  <div class="stage7-popup-grid__cell stage7-popup-grid__cell--5">' +
    '    <p class="stage7-popup-grid__header">Деловые обеды</p>' +
    "  </div>" +
    '  <div class="stage7-popup-grid__cell stage7-popup-grid__cell--6">' +
    '    <p class="stage7-popup-grid__lead">< 5800 руб.</p>' +
    "  </div>" +
    '  <div class="stage7-popup-grid__cell stage7-popup-grid__cell--7">' +
    '    <p class="stage7-popup-grid__lead"> ≥ 5800 руб.</p>' +
    "    <p>Требуется предварительное согласование непосредственного руководителя и внесение в реестр.</p>" +
    "  </div>" +
    '  <div class="stage7-popup-grid__cell stage7-popup-grid__cell--8">' +
    '    <p class="stage7-popup-grid__header">Мероприятия</p>' +
    "  </div>" +
    '  <div class="stage7-popup-grid__cell stage7-popup-grid__cell--9">' +
    '    <p class="stage7-popup-grid__lead">< 10900 руб.</p>' +
    "  </div>" +
    '  <div class="stage7-popup-grid__cell stage7-popup-grid__cell--10">' +
    '    <p class="stage7-popup-grid__lead">≥ 10900 руб.</p>' +
    '    <p class="stage7-popup-grid__lead">< 36300 руб.</p>' +
    "    <p>Требуется внесение в реестр.</p>" +
    "  </div>" +
    '  <div class="stage7-popup-grid__cell stage7-popup-grid__cell--11">' +
    '    <p class="stage7-popup-grid__lead">≥ 36300 руб.</p>' +
    "    <p>Требуется предварительное согласование непосредственного руководителя и внесение в реестр.</p>" +
    "  </div>" +
    '  <div class="stage7-popup-grid__cell stage7-popup-grid__cell--12">' +
    "    <p>Все лимиты указаны на одного чеолвека с учётом налогов.</p>" +
    "    <p>*Должностное лицо — лицо постоянно, временно или в соответствии со специальными полномочиями осуществляющее функции представителя власти, являющееся работником государственного / надзорного или контролирующего органа и т.п.; к категории «должностное лицо» также относятся должностные лица иностранного государства, члены королевских семей, должностные лица и работники публичной международной организации, государственной или контролируемой государством компании, политической партии, публичные должностные лица.</p>" +
    "  </div>" +
    "</section>"
  );
  // document
  //   .querySelector(".codecs__memo-link")
  //   .addEventListener("click", function (evt) {
  //     evt.preventDefault();
  //     evt.stopPropagation();
  //     tingleStage7.open();
  //   });
  tingleNextStage = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ["overlay", "button", "escape"],
    closeLabel: "Close",
    cssClass: ["stage7-modal"],
  });
  tingleNextStage.setContent(
    "Для прохождения этого этапа миссии вам необходимо пройти предыдущие шаги."
  );

  tingleNotAllAnswers = new tingle.modal({
    footer: false,
    stickyFooter: false,
    closeMethods: ["overlay", "button", "escape"],
    closeLabel: "Close",
    cssClass: ["stage7-modal"],
  });
  tingleNotAllAnswers.setContent(
    "Вам необходимо ответить на все вопросы, прежде чем двигаться дальше."
  );

  // scroll to element
  document.querySelectorAll(".scroll-down").forEach(function (node) {
    node.addEventListener("click", function (evt) {
      try {
        evt.preventDefault();
        window.scrollTo({
          top: getCoords(document.querySelector(this.dataset.scrollto)).top,
          behavior: "smooth",
        });
      } catch (err_) {
        console.log(err_);
      }
    });
  });

  // MAP
  // (function () {
  //   // var options = {
  //   //   rootMargin: '-200px'
  //   // };
  //   // timer
  //   var timerObserver = new IntersectionObserver(function (entries, tObserver) {
  //     entries.forEach(function (entry) {
  //       if (entry.isIntersecting) {
  //         var timer = entry.target;
  //         var cnt = timer.querySelector(".final__timer-disapear-countdown");

  //         var timeleft = 9;
  //         var downloadTimer = setInterval(function () {
  //           cnt.innerHTML = "00:0" + timeleft;
  //           timeleft -= 1;
  //           if (timeleft < -1) {
  //             timer.classList.add("visually-hidden");
  //             clearInterval(downloadTimer);
  //           }
  //           if (timeleft < 0) {
  //             // timer.classList.add('final__timer-disapear-countdown');
  //             document
  //               .querySelector(".final__timer")
  //               .classList.add("final__timer--hidden");
  //             cnt.innerHTML = "00:00";
  //           }
  //         }, 1000);
  //         tObserver.unobserve(timer);
  //       }
  //     });
  //   });
  //   timerObserver.observe(document.querySelector(".final__timer"));

  //   // var calendarHowTo = document.querySelector(".signup__calendar");
  //   // var calendarHowToClick = function(evt) {
  //   //   evt.preventDefault();
  //   //   var howTo = document.querySelector(".calendar-howto");
  //   //   howTo.classList.contains("visually-hidden")
  //   //     ? howTo.classList.remove("visually-hidden")
  //   //     : howTo.classList.add("visually-hidden");
  //   //   //document.querySelector('.calendar-howto').classList.remove('visually-hidden');
  //   //   // calendarHowTo.removeEventListener(calendarHowToClick);
  //   // };
  //   // calendarHowTo.addEventListener("click", calendarHowToClick);
  //   $(".signup__calendar").click(function (evt) {
  //     evt.preventDefault();
  //     $(".calendar-howto").toggleClass("visually-hidden");
  //   });
  // })();

  // STAGE 1

  // // Audio player
  // var timer;
  // var percent = 0;
  // var audio = document.getElementById("audio");
  // audio.addEventListener("playing", function (_event) {
  //   var duration = _event.target.duration;
  //   advance(duration, audio);
  // });
  // audio.addEventListener("pause", function (_event) {
  //   clearTimeout(timer);
  // });
  // var advance = function (duration, element) {
  //   var progress = document.getElementById("progress");
  //   increment = 10 / duration;
  //   percent = Math.min(increment * element.currentTime * 10, 100);
  //   progress.style.width = percent + "%";
  //   startTimer(duration, element);
  // };
  // var startTimer = function (duration, element) {
  //   if (percent < 100) {
  //     timer = setTimeout(function () {
  //       advance(duration, element);
  //     }, 100);
  //   }
  // };

  // function togglePlay(e) {
  //   e = e || window.event;
  //   var btn = e.target;
  //   if (!audio.paused) {
  //     btn.classList.remove("active");
  //     audio.pause();
  //     isPlaying = false;
  //   } else {
  //     btn.classList.add("active");
  //     audio.play();
  //     isPlaying = true;
  //   }
  // }
  // document
  //   .querySelector(".audio__toggler")
  //   .addEventListener("click", togglePlay);
  // // Audio player end

  function showBack(e) {
    e.target
      .closest(".flip-card_block")
      .classList.add("flip-card_block_rotate");
  }
  function showFront(e) {
    e.target
      .closest(".flip-card_block")
      .classList.remove("flip-card_block_rotate");
  }
  var frontCards = document.querySelectorAll(".flip-card_front");
  var backCards = document.querySelectorAll(".flip-card_back");
  try {
    frontCards.forEach(function (i) {
      i.addEventListener("click", showBack);
    });
    backCards.forEach(function (i) {
      i.addEventListener("click", showFront);
    });
  } catch (e) {
    console.log(e);
  }
  $(".flip__big").on("click", function () {
    if ($(".flip__big .flip-card_block_rotate").length) {
      $(".flip-card_center").hide();
    } else {
      $(".flip-card_center").show();
    }
  });

  $(".slider_wrapper").slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: false,
    // adaptiveHeight: true,
    centerPadding: "60px",
    fade: true,
    cssEase: "linear",
    autoplay: false,
    // autoplay: true,
    // autoplaySpeed: 1500,
    lazyLoad: "ondemand",
  });

  //Sortable block
  // activateSortable();
  /// Sortable end

  $(".test-btn_reply").on("click", handleDrop);
  function handleDrop(e) {
    var btn = this;
    var arrFalse = [];
    var _stage = "." + e.target.closest("article").classList[1];
    //console.log($(_stage + " .test_sort_answer_list li").length);
    if (typeof STATE.suspend_data.answered === "undefined") {
      STATE.suspend_data.answered = [];
    }
    if (
      !e.target
        .closest(".test-sort")
        .querySelectorAll(".test_sort_answer_list li").length
    ) {
      $(_stage + " .test_sort_cart li").each(function () {
        if (
          this.getAttribute("data-container") ==
          this.parentElement.getAttribute("data-container")
        ) {
          console.log("true : " + this);
          this.classList.add("drag_true");
        } else {
          console.log("false : " + this);
          this.classList.add("drag_false");
          arrFalse.push(this);
          btn.innerHTML = "Неверно";
          var elem = e.target
            .closest(".l-test")
            .querySelector('.l-row_container__p[data="' + false + '"]');
          if (elem.hasAttribute("data-id")) {
            elem = e.target
              .closest(".l-test")
              .querySelector('.l-row_container__p[data-id="' + el.id + '"]');
          }
          elem.style.display = "block";
        }
      });
      if (arrFalse.length) {
        btn.classList.add("bg_false");
        btn.innerHTML = "Неверно";
        var elem = e.target
          .closest(".l-test")
          .querySelector('.l-row_container__p[data="' + false + '"]');
        if (elem.hasAttribute("data-id")) {
          elem = e.target
            .closest(".l-test")
            .querySelector('.l-row_container__p[data-id="' + el.id + '"]');
        }
        elem.style.display = "block";
        $(this).closest(".test-sort").find(".test-btn-drop_check-true").show();
        // console.log(e.target.dataset.questionid);
        STATE.suspend_data.answered.push({
          questionId: e.target.dataset.questionid,
          part: STATE.currentPart,
          isCorrect: false,
        });
      } else {
        btn.classList.add("bg_true");
        btn.innerHTML = "Верно";
        var elem = e.target
          .closest(".l-test")
          .querySelector('.l-row_container__p[data="' + true + '"]');
        elem.style.display = "block";
        STATE.suspend_data.answered.push({
          questionId: e.target.dataset.questionid,
          part: STATE.currentPart,
          isCorrect: true,
        });
        STATE.score.raw = updateCompleteScore();
        STATE.score.scaled = calcScaledScore(STATE.score.raw);
      }
      $(this).off("click", handleDrop);
      this.classList.add("test-btn-active");
      saveStateToScorm();
    } else {
      console.log(
        e.target
          .closest(".test-sort")
          .querySelectorAll(".test_sort_answer_list li").length
      );
    }
  }

  $(".test_sort_item").on("drag", function () {
    this.classList.add("test_sort_item__dragend");
  });

  $(".test_sort_item").on("dragend", function () {
    if (this.closest(".test_sort_cart")) {
      this.classList.add("test_sort_item__dragend");
    } else {
      this.classList.remove("test_sort_item__dragend");
    }
  });

  $(".test-btn").on("click", getInp);

  function disabletAnswers() {
    if (STATE.suspend_data.answered) {
      STATE.suspend_data.answered.forEach(function (it) {
        try {
          it.answers.forEach(function (el) {
            var elem = document.querySelector("#" + el);
            $("#" + el).prop("checked", true);
            // debugger;
            disabletInputs(elem);
          });

          var btn = document
            .querySelector('div[data-questionid="' + it.questionId + '"]')
            .closest(".l-test")
            .querySelector(".test-btn");
        } catch (err) {
          var btn = document
            .querySelector('div[data-questionid="' + it.questionId + '"]')
            .closest(".test-sort")
            .querySelector(".test-btn_reply");
          var checkTrue = document
            .querySelector('div[data-questionid="' + it.questionId + '"]')
            .closest(".test-sort")
            .querySelector(".test-btn-drop_check-true");
        }
        if (!~btn.className.indexOf("test-btn-active"))
          btn.classList.add("test-btn-active");
        $(btn).off("click", getInp);
        $(btn).off("click", handleDrop);
        $(checkTrue).show();
        console.log($(btn));
        if (it.isCorrect) {
          btn.classList.add("bg_true");
          btn.innerHTML = "Верно";
        } else {
          btn.classList.add("bg_false");
          btn.innerHTML = "Неверно";
        }
      });
    }
  }

  try {
    disabletAnswers();
  } catch (err) {
    console.log(err);
  }

  function getInp(e) {
    var inputs = e.target.closest(".l-test").querySelectorAll("input:checked");
    var blockAnswer = e.target
      .closest(".l-test")
      .querySelectorAll(".l-test_answers");
    // var important = e.target.closest('.l-test').getAttribute('data-test');
    var result = [];
    // var cntAnswer = 0;
    if (inputs.length >= blockAnswer.length) {
      // STATE.cntBlockAnswer += blockAnswer.length;
      if (!~e.target.className.indexOf("test-btn-active"))
        e.target.classList.add("test-btn-active");
      e.target.removeEventListener("click", getInp);
      var queryInput = disabletInputs(e);
      console.log(e.target.dataset.questionid);
      if (typeof STATE.suspend_data.answered === "undefined") {
        STATE.suspend_data.answered = [];
      } else {
        var idx = STATE.suspend_data.answered.findIndex(function (
          it,
          idx,
          arr
        ) {
          return (
            parseInt(it.questionId, 10) ===
            parseInt(e.target.dataset.questionid, 10)
          );
        });
        if (idx >= 0) {
          STATE.suspend_data.answered.splice(idx, 1);
        }
      }

      // debugger;
      if (inputs[0].type == "checkbox") {
        var inputsAll = e.target.closest(".l-test").querySelectorAll("input");
        var _res = true;
        var arrAnswers = [];
        // console.log(inputs);
        inputsAll.forEach(function (el) {
          // debugger;
          console.log(el);
          if (el.checked && el.getAttribute("data-answer") != "true") {
            _res = false;
            el.nextElementSibling.classList.add("label-false");
            arrAnswers.push(el.id);
          } else if (!el.checked && el.getAttribute("data-answer") == "true") {
            _res = false;
            el.nextElementSibling.classList.add("label-false");
          }
          if (el.checked && el.getAttribute("data-answer") == "true") {
            el.nextElementSibling.classList.add("label-true");
            arrAnswers.push(el.id);
          }
        });
        if (_res) {
          STATE.suspend_data.answered.push({
            questionId: e.target.dataset.questionid,
            part: STATE.currentPart,
            isCorrect: true,
            answers: arrAnswers,
          });
          STATE.score.raw = updateCompleteScore();
          STATE.score.scaled = calcScaledScore(STATE.score.raw);
          e.target.classList.add("bg_true");
          e.target.innerHTML = "Верно";
          var elem = e.target
            .closest(".l-test")
            .querySelector('.l-row_container__p[data="' + true + '"]');
          elem.style.display = "block";
        } else {
          STATE.suspend_data.answered.push({
            questionId: e.target.dataset.questionid,
            part: STATE.currentPart,
            isCorrect: false,
            answers: arrAnswers,
          });
          e.target.classList.add("bg_false");
          e.target.innerHTML = "Неверно";
          var elem = e.target
            .closest(".l-test")
            .querySelector('.l-row_container__p[data="' + false + '"]');
          if (elem.hasAttribute("data-id")) {
            elem = e.target
              .closest(".l-test")
              .querySelector('.l-row_container__p[data-id="' + el.id + '"]');
          }
          elem.style.display = "block";
        }
      } else {
        var arrAnswers = [];
        inputs.forEach(function (el) {
          console.log(el.id);
          // debugger;
          if (el.getAttribute("data-answer") == "true") {
            // if(important){ STATE.score++;}
            result.push(el);
            e.target.classList.add("bg_true");
            e.target.innerHTML = "Верно";
            var elem = e.target
              .closest(".l-test")
              .querySelector('.l-row_container__p[data="' + true + '"]');
            elem.style.display = "block";
            // STATE.score.raw += 1;
            arrAnswers.push(el.id);
            STATE.suspend_data.answered.push({
              questionId: e.target.dataset.questionid,
              part: STATE.currentPart,
              isCorrect: true,
              answers: arrAnswers,
            });
            STATE.score.raw = updateCompleteScore();
            STATE.score.scaled = calcScaledScore(STATE.score.raw);
            // updateCompleteScore();
          } else {
            el.nextElementSibling.classList.add("label-false");
            e.target.classList.add("bg_false");
            e.target.innerHTML = "Неверно";
            var elem = e.target
              .closest(".l-test")
              .querySelector('.l-row_container__p[data="' + false + '"]');
            if (elem.hasAttribute("data-id")) {
              elem = e.target
                .closest(".l-test")
                .querySelector('.l-row_container__p[data-id="' + el.id + '"]');
            }
            elem.style.display = "block";
            arrAnswers.push(el.id);
            STATE.suspend_data.answered.push({
              questionId: e.target.dataset.questionid,
              part: STATE.currentPart,
              isCorrect: false,
              answers: arrAnswers,
            });
          }
          // pipwerks.SCORM.data.set('cmi.suspend_data', JSON.stringify(STATE.suspend_data));
          // pipwerks.SCORM.save();
          // saveStateToScorm();
          el.nextElementSibling.classList.add("label-true");
        });
      }
      saveStateToScorm();

      // console.log(result.length == inputs.length);

      if (result.length == inputs.length) {
        var elem = e.target
          .closest(".l-test")
          .querySelector('.l-row_container__p[data="true"]');
      } else {
        var elem = e.target
          .closest(".l-test")
          .querySelector('.l-row_container__p[data="false"]');
      }
      console.log(elem);

      // if(result.length == inputs.length){
      //   var elem = e.target.closest('.l-test').querySelector('.l-row_container__p[data="'+true+'"]');
      // }else if(important && result.length > 1) {
      //   var elem = e.target.closest('.l-test').querySelector('.l-row_container__p[data="'+true+'"]');
      // } else {
      //   var elem = e.target.closest('.l-test').querySelector('.l-row_container__p[data="'+false+'"]');
      // }

      // elem.style.display = 'block';

      // if(result.length == inputs.length){
      //   var elem = e.target.closest('.l-test').querySelector(".right_answer");
      //   elem.hidden = false;
      //   $('html, body').animate({
      //     scrollTop: getOffsetRect(elem).top - 100
      //   }, 1000);
      //   //myScrollTo(elem);
      //
      //
      // }else if(important && result.length > 1) {
      //   var elem = e.target.closest('.l-test').querySelector(".right_answer");
      //   elem.hidden = false;
      //   $('html, body').animate({
      //     scrollTop: getOffsetRect(elem).top - 100
      //   }, 1000);
      //   // myScrollTo(elem);
      // } else {
      //   var elemF = e.target.closest('.l-test').querySelector(".false_answer");
      //   elemF.hidden = false;
      //   $('html, body').animate({
      //     scrollTop: getOffsetRect(elemF).top - 100
      //   }, 1000);
      //   //myScrollTo(elemF);
      // }
    } else {
      console.log(
        ">>>>>> BAD BAD BAD :( !!!!",
        inputs.length,
        blockAnswer.length
      );
    }
  }
  function disabletInputs(e) {
    try {
      var inputs = e.target
        ? e.target.closest(".l-test").querySelectorAll("input")
        : e.closest(".l-test").querySelectorAll("input");
    } catch (err) {
      var inputs = e.closest(".l-test").querySelectorAll("input");
    }
    inputs.forEach(function (el) {
      el.setAttribute("disabled", "true");
      if (el.getAttribute("data-answer") == "true")
        el.nextElementSibling.classList.add("label-true");
    });
    return inputs.length;
  }
  //$(this).closest('.test-sort').find(".test-btn-drop_check-true")
  $(".test-btn-drop_check-true").on("click", showTrueDrop);
  function showTrueDrop() {
    var dropList = $(this).closest(".test-sort").find("li");
    $(this)
      .closest(".test-sort")
      .find(".test_sort_cart")
      .each(function () {
        var cart = this;
        dropList.each(function () {
          console.log(
            this.getAttribute("data-container") +
            "  =  " +
            cart.getAttribute("data-container")
          );
          if (
            this.getAttribute("data-container") ==
            cart.getAttribute("data-container")
          ) {
            console.log(
              this.getAttribute("data-container") +
              "  =  " +
              cart.getAttribute("data-container")
            );
            this.classList.remove("drag_true", "drag_false");
            cart.append(this);
          }
        });
      });
  }
});

var bClose = document.querySelector(".button-close__main");

bClose.onclick = function () {
  window.close();
};
// IE11 picture tag support
//=require node_modules/picturefill/dist/picturefill.min.js
// Tingle modal
//=require node_modules/tingle.js/dist/tingle.min.js

// //=require node_modules\lettering\dist\lettering.min.js
//=require node_modules/slick-carousel/slick/slick.min.js
// //=require node_modules/sortablejs/Sortable.min.js

// //=require node_modules/lory.js/dist/jquery.lory.js

// audio player
var timer;
var percent = 0;
var advance = function (duration, element) {
  var progress = element.nextElementSibling.querySelector(".audio__progress");
  increment = 10 / duration;
  percent = Math.min(increment * element.currentTime * 10, 100);
  progress.style.width = percent + "%";
  startTimer(duration, element);
};
var startTimer = function (duration, element) {
  if (percent < 100) {
    timer = setTimeout(function () {
      advance(duration, element);
    }, 100);
  }
};

function togglePlay(e, audio) {
  e = e || window.event;
  var btn = e.target;
  if (!audio.paused) {
    btn.classList.remove("active");
    audio.pause();
    isPlaying = false;
  } else {
    btn.classList.add("active");
    audio.play();
    isPlaying = true;
  }
}
//end audio player

document.onclick = function (e) {
  if (e.target.classList.contains("video__button")) {
    e.preventDefault();
    var v = e.target.closest(".video").querySelector(".video__main");
    var speed = Number(e.target.dataset.speed);
    if (e.target.dataset.status == "normal") {
      v.playbackRate = speed;
      e.target.innerHTML = "Вернуть обычную скорость";
      e.target.setAttribute("data-status", "speed");
    } else {
      e.target.innerHTML = "Ускорить видео";
      v.playbackRate = 1.0;
      e.target.setAttribute("data-status", "normal");
    }
  }

  if (e.target.classList.contains("audio__toggler")) {
    e.preventDefault();
    var audio = e.target.closest(".audio").querySelector("audio");
    audio.addEventListener("playing", function (_event) {
      var duration = _event.target.duration;
      advance(duration, audio);
    });
    audio.addEventListener("pause", function (_event) {
      clearTimeout(timer);
    });
    togglePlay(e, audio);
  }
};
