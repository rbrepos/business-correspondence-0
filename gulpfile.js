"use strict";

var gulp = require("gulp");
var sass = require("gulp-sass");
var sourcemaps = require("gulp-sourcemaps");
var plumber = require("gulp-plumber");
var rename = require("gulp-rename");
var postcss = require("gulp-postcss");
var autoprefixer = require("autoprefixer");
var csso = require("gulp-csso");
var imagemin = require("gulp-imagemin");
var webp = require("gulp-webp");
var svgstore = require("gulp-svgstore");
var posthtml = require("gulp-posthtml");
var include = require("posthtml-include");
var del = require("del");
var server = require("browser-sync").create();
// var concat = require('gulp-concat');
var includejs = require("gulp-include");
var uglify = require("gulp-uglify");
var htmlmin = require("gulp-htmlmin");
var gulpFileList = require("gulp-filelist");
const fileinclude = require("gulp-file-include");
const zip = require("gulp-zip");

gulp.task("minifyhtml", function () {
  return gulp
    .src("build/res/*.html")
    .pipe(htmlmin({ collapseWhitespace: false }))
    .pipe(gulp.dest("build/res"));
});

gulp.task("concat-scripts", function () {
  return gulp
    .src("source/js/main.js")
    .pipe(
      includejs({
        extensions: "js",
        includePaths: [__dirname],
      })
    )
    .on("error", console.log)
    .pipe(rename("app.js"))
    .pipe(gulp.dest("build/res/js"));
});

gulp.task("compressjs", function () {
  return (
    gulp
      .src("build/res/js/app.js")
      // .pipe(uglify())
      .pipe(rename("app.min.js"))
      .pipe(gulp.dest("build/res/js"))
  );
});

gulp.task("js", gulp.series("concat-scripts", "compressjs"));

gulp.task("images", function () {
  return gulp
    .src("source/img/**/*.{png,jpg,svg}")
    .pipe(
      imagemin([
        imagemin.optipng({ optimizationLevel: 3 }),
        imagemin.jpegtran({ progressive: true }),
        imagemin.svgo(),
      ])
    )
    .pipe(gulp.dest("source/img"));
});

gulp.task("webp", function () {
  return gulp
    .src("source/img/**/*.{png,jpg}")
    .pipe(webp({ quality: 85 }))
    .pipe(gulp.dest("source/img"));
});

gulp.task("sprite", function () {
  return gulp
    .src("source/img/*.svg")
    .pipe(
      svgstore({
        inlineSvg: true,
      })
    )
    .pipe(rename("sprite.svg"))
    .pipe(gulp.dest("build/res/img"));
});

gulp.task("html", function () {
  return gulp
    .src("source/*.html")
    .pipe(posthtml([include()]))
    .pipe(gulp.dest("build/res"));
});

gulp.task("manifest", function () {
  return gulp
    .src("build/imsmanifest.xml")
    .pipe(
      posthtml([include({ encoding: "utf8" })])
      // .process(html)
    )
    .pipe(gulp.dest("build"));
});

gulp.task("copy", function () {
  return gulp
    .src(
      [
        "source/fonts/**/*.{woff,woff2,OTF}",
        "source/img/**",
        "source/js/**",
        "source/media/**",
        "source/humans.txt",
        "source/favicon.ico",
      ],
      {
        base: "source",
      }
    )
    .pipe(gulp.dest("build/res"));
});

gulp.task("copy-manifest", function () {
  return gulp
    .src(
      [
        "source/imsmanifest.xml",
        // 'source/starting_zip.sh'
      ],
      {
        base: "source",
      }
    )
    .pipe(gulp.dest("build"));
});

// gulp.task('copy_wp', function () {
//   return gulp.src([
//       'build/fonts/**/*.{woff,woff2}',
//       'build/img/**',
//       'build/js/**',
//       'source/media/**',
//       'build/css/**',
//       'build/*.html',
//       'build/humans.txt'
//     ], {
//       base: 'build'
//     })
//     .pipe(gulp.dest('wp-content/themes/paradoxprava/assets'));
// });

gulp.task("clean", function () {
  return del("build");
});

// gulp.task('clean_wp', function () {
//  return del('wp-content/themes/paradoxprava/assets');
// });

gulp.task("css", function () {
  return (
    gulp
      .src("source/sass/style.scss")
      .pipe(sourcemaps.init())
      .pipe(plumber())
      .pipe(sass({ includePaths: require("node-normalize-scss").includePaths }))
      //.pipe(sourcemaps.write({includeContent: false}))
      //.pipe(sourcemaps.init({loadMaps: true}))
      .pipe(postcss([autoprefixer()]))
      //.pipe(sourcemaps.write('.'))
      .pipe(gulp.dest("build/res/css"))
      .pipe(csso())
      //.pipe(sourcemaps.write('./build/css'))
      .pipe(rename("style.min.css"))
      .pipe(gulp.dest("build/res/css"))
      .pipe(server.stream())
  );
});

gulp.task("server", function () {
  server.init({
    server: "build/res",
    notify: false,
    open: true,
    cors: true,
    ui: false,
  });
  gulp.watch("source/sass/**/*.{scss,sass}", gulp.series("css"));
  // gulp.watch('source/js/*.js', gulp.series('js', 'html', 'minifyhtml', 'refresh'));
  gulp.watch("source/js/*.js", gulp.series("js", "html", "refresh"));
  // gulp.watch('source/img/icon-*.svg', gulp.series('sprite', 'html', 'minifyhtml', 'refresh'));
  gulp.watch("source/img/icon-*.svg", gulp.series("sprite", "html", "refresh"));
  // gulp.watch('source/*.html', gulp.series('html', 'minifyhtml', 'refresh'));
  gulp.watch("source/*.html", gulp.series("html", "refresh"));
});

gulp.task("refresh", function (done) {
  server.reload();
  done();
});

function formatter(filePath) {
  var filePathArr = filePath.split("/");
  return (
    '<file href="' +
    filePathArr.splice(1, filePathArr.length).join("/") +
    '"/>\n'
  );
  // return 1;
}

gulp.task("file-list", function () {
  return gulp
    .src("build/res/**/*")
    .pipe(gulpFileList("filelist.xml", { destRowTemplate: formatter }))
    .pipe(gulp.dest("build"));
});

gulp.task("fileinclude", function () {
  return gulp
    .src("build/imsmanifest.xml")
    .pipe(fileinclude())
    .pipe(gulp.dest("build"));
});

gulp.task("clean-build", function () {
  return del([
    "build/css",
    "build/filelist.xml",
    "build/fonts",
    "build/img",
    "build/js",
    "build/media",
  ]);
});

gulp.task("zip-scorm", function () {
  return gulp
    .src("build/**")
    .pipe(zip("starting.zip"))
    .pipe(gulp.dest("build"));
});

gulp.task("img", gulp.series("images", "webp", "sprite", "copy"));
//gulp.task('build', gulp.series('clean', 'copy', 'css', 'sprite', 'js', 'html', 'minifyhtml', 'clean_wp', 'copy_wp'));
gulp.task(
  "build",
  gulp.series(
    "clean",
    "copy",
    "css",
    "sprite",
    "js",
    "html",
    "minifyhtml",
    "copy-manifest",
    "file-list",
    "fileinclude",
    "clean-build"
  )
);
// gulp.task('build', gulp.series('clean', 'copy', 'css', 'sprite', 'js', 'html', 'copy-manifest', 'file-list', 'fileinclude', 'clean-build'));
//gulp.task('deploy', gulp.series('build', 'publish'));
gulp.task("start", gulp.series("build", "server"));
